<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
	<title>DataTables example - Scroll - horizontal and vertical</title>
	<link rel="stylesheet" type="text/css" href="../media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="resources/syntax/shCore.css">
	<link rel="stylesheet" type="text/css" href="resources/demo.css">

	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.3.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="../media/js/jquery.dataTables.js">
	</script>
	<script type="text/javascript" language="javascript" src="../extensions/ColReorder/js/dataTables.colReorderWithResize.js">
	</script>
	<script type="text/javascript" language="javascript" src="../extensions/FixedColumns/js/dataTables.fixedColumns.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="resources/syntax/shCore.js">
	</script>
	<script type="text/javascript" language="javascript" class="init">
	
$(document).ready(function() {
	var oTable = $('#example').DataTable( {
		dom: "Rlfrtip",
		scrollY: 300,
		scrollX: true,
		"sScrollY": "300px",
        "sScrollX": "100%",
        "sScrollXInner": "150%",
        "bScrollCollapse": true
	} );
	new $.fn.dataTable.FixedColumns( oTable );
} );

	</script>
	<style type="text/css">
		/*th { border-right: 1px solid black; }*/
		.fixname {
			border-right: 0;
			z-index: 99;
			background-color: white;
		}
	</style>
</head>
<body class="dt-example">
	<div class="container">
		<section>
			<h1>DataTables example <span>Scroll - horizontal and vertical</span></h1>
			<table id="example" class="display nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
			            <th rowspan="3" class="fixname">Name</th>
			            <th colspan="3">HR Information</th>
			            <th colspan="5">Contact</th>
			        </tr>
			        <tr>
						<th>Start date</th>
						<th>Position</th>
						<th colspan="2">Office</th>
						<th>Age</th>
						<th>Salary</th>
						<th>Extn.</th>
						<th>Phone</th>
						<th>E-mail</th>
			        </tr>
			        <tr>
			        	<th colspan="2"></th>
			        	<th>Center</th>
			        	<th>Branch</th>
			        	<th colspan="5"></th>
			        </tr>
				</thead>
				<tbody>
					<?php
						for ($i=0; $i < 20; $i++) { 
							$firstname = array("Tiger","Garrett","Ashton","Airi","Brielle","John","Ali","James","Mark","Jill");
							$lastname = array("Nixon","Winters","Kelly","Cox","Williamson","Webber","Dason","Meilany","Dempel","Hover");
							$date = array("2011/04/25","2011/04/04","2011/07/16","2012/11/17","2012/08/20");
							$position = array("System Architect","Web Programmer","Web Design","Systems Analyst","Java Developer");
							$centeroffice = array("New York","Washington","Settle","Log Angeles","Chicago");
							$branchoffice = array("Las Vegas","Atlanta","Boston","Detroit","Denver");
							$shufflename = array($firstname[rand(0,9)], $lastname[rand(0,9)]);
							$fixfirstname = $shufflename[rand(0,1)]; 
							$fixlastname = ($shufflename[0] == $fixfirstname ? $shufflename[1] : $shufflename[0]);
							echo '
							<tr>
								<td>'.$fixfirstname.' '.$fixlastname.'</td>
								<td>'.$date[rand(0,4)].'</td>
								<td>'.$position[rand(0,4)].'</td>
								<td>'.$centeroffice[rand(0,4)].'</td>
								<td>'.$branchoffice[rand(0,4)].'</td>
								<td>'.rand(24,55).'</td>
								<td>$'.number_format(rand(145000,335000), 0, '.', ',').'</td>
								<td>'.rand(2394,5632).'</td>
								<td>0815'.rand(1234567,3453734).'</td>
								<td>'.strtolower(substr($fixfirstname, 0, 1)).'.'.strtolower($fixlastname).'@datatables.net</td>
							</tr>
							';
						}
					?>
				</tbody>
			</table>
			</div>
		</section>
	</div>
</body>
</html>